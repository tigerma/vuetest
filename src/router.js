
import Vue from "vue";
import Router from "vue-router";
//引入在路由中需要用到的组件

import User from './components/user/User.vue';
import Bar from './components/Bar.vue';
import UserProfile from './components/user/UserProfile';
import UserPosts from './components/user/UserPosts';



//第二步:加载Router
Vue.use(Router);//加载全局组件Router

//第三步:配置路由实例
const router = new Router({
	//mode:'history', //路由模式:默认为hash,如果改为history,则需要后端进行配合
	//base:'/',//基路径:默认值为'/'.如果整个单页应用在/app/下,base就应该设为'/app/'.一般可以写成__dirname,在webpack中配置.
	routes: [
		{
			path: '/user/:id',
			name: 'user', //给路由命名,设置的name要唯一!
			component: User,//就是第一步import的组件
			children: [
				{
					path: 'profile',
					name: 'profile',
					component: 	UserProfile
				},
				{
					path: 'posts',
					name: 'posts',
					component: UserPosts
				}
			]
		},
		{
			path: '/bar/:id',
			name: 'bar', //给路由命名,设置的name要唯一!
			component: Bar//就是第一步import的组件
		}
	]
})

export default router;